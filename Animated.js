import React, {useState, useEffect, useRef, createRef} from 'react';
import {
  Animated,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Platform,
} from 'react-native';

const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 60;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const getRandomData = (prefix, page) => {
  const size = 20;
  return new Array(size).fill('').map((item, index) => {
    return {
      title: `${prefix} ${page * size + index + 1}`,
      id: page * size + index + 1,
    };
  });
};

const App = () => {
  const scrollYRef = useRef(new Animated.Value(0)).current;
  const hideRef = useRef(new Animated.Value(0)).current;
  const prevScrollYRef = useRef(0);

  const flRef = useRef();

  const [fl1, setFl1] = useState(getRandomData('Item', 0));

  const pageRef = useRef(0);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [hasMore, setHasMore] = useState(false);

  const headerHeight = hideRef.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
    extrapolate: 'clamp',
  });

  // useEffect(() => {
  //   return () => {
  //     scrollYRef.current.removeAllListeners();
  //   };
  // }, []);

  const onScroll = () => {
    return Animated.event([{nativeEvent: {contentOffset: {y: scrollYRef}}}], {
      useNativeDriver: false,
      listener: () => {
        let val = hideRef._value + (scrollYRef._value - prevScrollYRef.current);

        if (val < 0) {
          val = 0;
        } else if (val > HEADER_SCROLL_DISTANCE) {
          val = HEADER_SCROLL_DISTANCE;
        }

        hideRef.setValue(val);
        prevScrollYRef.current = scrollYRef._value;
      },
    });
  };

  const renderItem = ({title, id}) => {
    return (
      <View key={id} style={styles.row}>
        <Text>{title}</Text>
      </View>
    );
  };

  const handlePullToRefresh = () => {
    pageRef.current = 0;
    setIsRefreshing(true);
    setTimeout(() => {
      setFl1(getRandomData('refreshed', 0));
      setIsRefreshing(false);
    }, 1000);
  };

  const onEndReached = () => {
    if (pageRef.current === 0) {
      pageRef.current = 1;
      setHasMore(true);
      setTimeout(() => {
        setFl1([...fl1, ...getRandomData('abc', pageRef.current)]);
        setHasMore(false);
      }, 1000);
    }
  };

  // console.log(headerHeight);
  return (
    <View style={styles.fill}>
      <FlatList
        ref={flRef}
        data={fl1}
        onScroll={onScroll()}
        // ListHeaderComponent={<View style={{height: HEADER_MAX_HEIGHT}} />}
        ListHeaderComponent={
          <View>
            <Text>This is List Header</Text>
          </View>
        }
        contentContainerStyle={{
          paddingTop: Platform.OS === 'ios' ? 0 : HEADER_MAX_HEIGHT,
        }}
        scrollEventThrottle={1}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => renderItem(item)}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={handlePullToRefresh}
            progressViewOffset={HEADER_MAX_HEIGHT}
          />
        }
        ListFooterComponent={
          hasMore && (
            <View style={{flex: 1, justifyContent: 'center'}}>
              <ActivityIndicator />
            </View>
          )
        }
        //iOS
        contentInset={{
          top: HEADER_MAX_HEIGHT,
        }}
        contentOffset={{
          y: -HEADER_MAX_HEIGHT,
        }}
        // automaticallyAdjustContentInsets={false}
      />
      <Animated.View style={[styles.header, {height: headerHeight}]}>
        <View
          style={{
            width: '100%',
            height: HEADER_SCROLL_DISTANCE,
            backgroundColor: 'red',
            position: 'absolute',
            bottom: HEADER_MIN_HEIGHT,
          }}
        />
        <View style={styles.bar}>
          <TouchableOpacity
            onPress={() => {
              if (flRef.current)
                flRef.current.scrollToOffset({animated: true, offset: 0});
            }}>
            <Text style={styles.title}>Title</Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  fill: {
    flex: 1,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#03A9F4',
    overflow: 'hidden',
  },
  bar: {
    width: '100%',
    marginTop: 28,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
  },
  title: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: 18,
  },
  scrollViewContent: {
    marginTop: HEADER_MAX_HEIGHT,
  },
});

export default App;
