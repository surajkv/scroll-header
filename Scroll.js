import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  StyleSheet,
  Dimensions,
} from 'react-native';

const getRandomData = (prefix, page) => {
  const size = 40;
  return new Array(size).fill('').map((item, index) => {
    return {
      title: `${prefix} ${page * size + index + 1}`,
      id: page * size + index + 1,
    };
  });
};

const App = () => {
  const [isRefreshing, setIsRefreshing] = useState(false);

  const renderItem = ({title, id}) => {
    return (
      <View key={id} style={styles.row}>
        <Text>{title}</Text>
      </View>
    );
  };

  return (
    <View>
      <ScrollView nestedScrollEnabled={true} scrollEnabled={false}>
        <View style={styles.bar}>
          <Text style={styles.title}>Title</Text>
        </View>
        <FlatList
          nestedScrollEnabled={true}
          data={getRandomData('Item', 0)}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => renderItem(item)}
          style={{height: Dimensions.get('window').height - 100}}
        />
        <View style={styles.bar}>
          <Text style={styles.title}>Title</Text>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  fill: {
    flex: 1,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bar: {
    width: '100%',
    marginTop: 28,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    backgroundColor: 'transparent',
    color: 'red',
    fontSize: 18,
  },
});

export default App;
